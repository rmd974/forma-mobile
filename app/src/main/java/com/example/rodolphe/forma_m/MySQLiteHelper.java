package com.example.rodolphe.forma_m;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "forma";

    private static final int DATABASE_VERSION = 1;

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_USER_TABLE = "CREATE TABLE user ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "login TEXT, " +
                "password TEXT )";

        String CREATE_DOMAIN_TABLE = "CREATE TABLE domain ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT )";

        database.execSQL(CREATE_USER_TABLE);
        database.execSQL(CREATE_DOMAIN_TABLE);

        // Creation d'un jeu d'essai
        database.execSQL("INSERT INTO users VALUES(1,'demo','secret')");

        database.execSQL("INSERT INTO users VALUES(1,'demo','secret')");
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS users");
        onCreate(database);
    }

    // Méthodes sur la base de données.

    // Méthode de connexion.
    public boolean login(String login, String password) {
        boolean success = false;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = db.query("user", new String[]{"login", "password"}, "login = ? AND password = ?", new String[]{login, password}, null, null, null);

        //Si il y a un jeu de résultats.
        if (mCursor.getCount() > 0) {
            success = true; //La connexion est réussie.
        }

        //Fermeture du curseur.
        mCursor.close();
        db.close();

        return success;
    }

    //Récupération des domaines.
    public ArrayList getDomains() {
        ArrayList domains = new ArrayList();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM domain", null);

        if(mCursor.moveToFirst()){
            do{
                Domain domain = new Domain(mCursor.getInt(0), mCursor.getString(1));
                domains.add(domain);
            }while(mCursor.moveToNext());
        }

        mCursor.close();
        db.close();

        return domains;
    }


}