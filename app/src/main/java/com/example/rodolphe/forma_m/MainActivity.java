package com.example.rodolphe.forma_m;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.lang.*;
import java.sql.SQLException;

import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Déclaration des éléments à  utiliser.
        final EditText ztLogin = (EditText) findViewById(R.id.ztLogin);
        final EditText ztPassword = (EditText) findViewById(R.id.ztPassword);
        final Button btConnect = (Button) findViewById(R.id.btConnect);

        final MySQLiteHelper myDbHelper = new MySQLiteHelper(this);

        //Clic du bouton login.
        btConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Les champs ne sont pas vides.
                if (!(ztLogin.toString().trim().isEmpty()) && !(ztPassword.toString().trim().isEmpty())) {
                    if (myDbHelper.login(ztLogin.toString(), ztPassword.toString()) == true) {
                        Toast.makeText(MainActivity.this, "Connexion réussie.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Identifiants incorrects.", Toast.LENGTH_SHORT).show();
                    }

                } else {//Les champs sont vides.
                    Toast.makeText(MainActivity.this, "Tous les champs sont obligatoires.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
